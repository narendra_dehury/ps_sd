

/*******************    PS_SD.h v 1.0.1   ****************************

AUTHOR : Isasense 

CREATED : 5th July 2015

WORKS FOR ARDUINO MEGA ------> Made for Pollustion Sensor Data Logging.

Library Used - Adafruit SD Library ----->    https://github.com/adafruit/SD


include the two libraries before including this one

#include <SPI.h>
#include <SD.h>


*/


class SDextended
{
  private:
 
  	// logFile stores the logs
    // logDetailsFile stores the no. of logs currently stored in the logFile
  	char* _logFile = "Log.txt";
  	char* _logDetailsFile = "LogDetailsFile.txt";


  public:
    SDextended();
    char SDinit(char use_default_pins = 0);
    char SDcreate(char* filename);
    char SDdelete(char* filename);
    char SDlog(char* filename,char* log_text);
    void SDsee(char* filename);
    void SDreadplus(char* filename,void (*call)(char));
    String SDreadline(File &dataFile); // pass by reference

    //// HIGH LEVEL LOG FUNCTIONS ////
    
    char LOGinit();
    char LOGpush(char* data2log);
	char LOGpop(unsigned char lines2pop,void (*call)(String));

    
    

};


SDextended::SDextended()
{

}


char SDextended::SDinit(char use_default_pins)
{

  //const int chipSelect = 10;    
  //int _mosi=8;
  //int _miso=7;
  //int _sck=9;


  const int chipSelect = 53;    
  int _mosi= 51;
  int _miso= 50;
  int _sck= 52;

  String _reporter = "SDinit: "; // name of thi sfunction for debugging and printing into serial trerminal


  Serial.print(_reporter );
  Serial.println(F(" Initializing SD card..."));
 
   pinMode(SS, OUTPUT); 
   
  if(use_default_pins==0)
  {
  	if (!SD.begin(chipSelect,_mosi,_miso,_sck)) 
  	{
    	Serial.println(F("initialization failed!"));
    	return 0;
  	}
  } 
  else
  {
  	if (!SD.begin(chipSelect)) 
  	{
    	Serial.println(F("initialization failed!"));
    	return 0;
  	}
  }
  

  Serial.print(_reporter);
  Serial.println(F(" initialization done."));
  return 1;

}

char SDextended::SDcreate(char* filename)
{
  String _reporter = "SDcreate: "; // name of thi sfunction for debugging and printing into serial trerminal

  if(SD.exists(filename))
  {
    Serial.print(_reporter);
    Serial.print(F(" File already exists... name of file ---> "));
    Serial.println( String(filename));
    return 2;
  }


	File dataFile = SD.open(filename,FILE_WRITE);
	dataFile.close();

	if (SD.exists(filename)) 
    {
      Serial.print(_reporter);
      Serial.print(filename);
      Serial.println(F(" was created successfully."));
      return 1;
    }

	else return 0;
}




char SDextended::SDdelete(char* filename)
{
  String _reporter = "SDdelete: "; // name of thi sfunction for debugging and printing into serial trerminal

	if(SD.exists(filename))
	{
		Serial.print(_reporter);
    Serial.print(F(" File found . Deleting.... "));
    Serial.println(String(filename));
		SD.remove(filename);
	}
	else
	{
		Serial.print(_reporter);
    Serial.print(F(" File "));
    Serial.print(String(filename));
    Serial.println(F(" not found."));
		return 0;
	}

	if (SD.exists(filename)) return 0;
	else
	{
		Serial.print(_reporter +  String(filename));
    Serial.println(F(" deleted successfully."));
		return 1;
	}
}




char SDextended::SDlog(char* filename,char* log_text)
{

  String _reporter = "SDlog: "; // name of thi sfunction for debugging and printing into serial trerminal

	if(SD.exists(filename))
	{
		Serial.print(_reporter);
    Serial.print(F(" File found ... "));
    Serial.print(String(filename));
    Serial.print(F("   Logging -->  "));
    Serial.println(String(log_text));
	}
	else
	{
		Serial.print(_reporter);
    Serial.print(F(" File "));
    Serial.print(String(filename));
    Serial.println(F(" not found."));
		return 0;
	}

	File dataFile = SD.open(filename,FILE_WRITE);

	dataFile.println(log_text);

	dataFile.close();

	return 1;
}



void SDextended::SDsee(char* filename)
{
  String _reporter = "SDsee: "; // name of thi sfunction for debugging and printing into serial trerminal

	if(SD.exists(filename))
	{
		Serial.print(_reporter);
    Serial.print(F(" File "));
    Serial.print(String(filename));
    Serial.println(F(" found. Reading...."));

		File dataFile = SD.open(filename);
  

  		// if the file is available, read from it:
  		if (dataFile) 
  		{
    		while (dataFile.available()) 
    		{
      			Serial.write(dataFile.read());
      		}
      	}
      	else
      	{
      		Serial.print(_reporter);
          Serial.print(F(" Couldn't open file "));
          Serial.println(String(filename));
      	}	
    
   		dataFile.close();
    }

    else
    {
    	Serial.print(_reporter);
      Serial.print(F(" File "));
      Serial.print(String(filename));
      Serial.println(F(" not found."));
    }
    
 } 



 void SDextended::SDreadplus(char* filename,void (*call)(char))
 {
  String _reporter = "SDreadplus: "; // name of thi sfunction for debugging and printing into serial trerminal

 	if(SD.exists(filename))
	{
		Serial.print(_reporter + " File " + String(filename));
    Serial.println(F(" found. Reading...."));

		File dataFile = SD.open(filename);
  

  		// if the file is available, read from it:
  		if (dataFile) 
  		{
    		while (dataFile.available()) 
    		{
      			call(dataFile.read());
      	}
      }
      else
      {
      	Serial.print(_reporter);
        Serial.print(F(" Couldn't open file "));
        Serial.println(String(filename));
      }	
    
   		dataFile.close();
    }

  else
  {
    	Serial.println(_reporter + " File " + String(filename) + " not found.");
  }
 }



String SDextended::SDreadline(File &dataFile) // takes the file handle as the arguement , pass by reference
 {

  String _reporter = "SDreadline: "; // name of thi sfunction for debugging and printing into serial trerminal

  String line = ""; // take care of ram too.



      // if the file is available, read from it:
      if (dataFile) 
      {
        while (dataFile.available()) 
        {
            char r = dataFile.read();

            if(r== EOF) break; // end of file

            line += r ; // append or concatenate with the string , IMPORTANT might need to convert r to string before appending like String(r) , but seems 2 work w/o it.

            if(r=='\n') break;// if line ending then break loop

        }
      }
      else
      {
        Serial.print(_reporter);
        Serial.println(F(" Couldn't read file."));
      } 

    return line;
 }


//////////////////////////////////////////////////////  HIGHER LEVEL LOGGING FUNCTIONS ///////////////////////////////////////////////////
char SDextended::LOGinit()
{
	
	// renew/empty the logfile on initialisaion
	if( !SD.exists(_logFile) || SDdelete(_logFile) && SDcreate(_logFile) );
	else 	// if any one of above fails then return 0
	{
		Serial.println(F("LOG initialisation failed"));
		return 0;	
	}

	// renew/empty the logDetailsfile on initialisaion
	// write 0 to logDetails file as no logs have been stored n logFile yet
	if( !SD.exists(_logDetailsFile) || SDdelete(_logDetailsFile) && SDcreate(_logDetailsFile) && SDlog(_logDetailsFile,"0")); 
	else	// if any one of above fails then return 0
	{
		Serial.println(F("LOG initialisation failed"));
		return 0;	
	}

	Serial.println(F("LOG initialised successfully"));
	return 1;
}

char SDextended::LOGpush(char* data2log)
{
	return SDlog(_logFile,data2log);
}

// POP Lines function

#define POP_MAX_LINES	20

//default callback function
void doNothing(String line)
{
	return;
}

char SDextended::LOGpop(unsigned char lines2pop,void (*call)(String)=doNothing)
{
	if(!SD.exists(_logFile))
	{
		Serial.println(F("logFile not found. Please call LOGinit() first."));
	}

	if(lines2pop > POP_MAX_LINES)
	{
		Serial.print(F("Can't pop more than "));
    Serial.print(String(POP_MAX_LINES));
    Serial.println(F(" lines at a time."));
		return 0;
	}

	File file = SD.open(_logFile);
	
	for (int i = 0; i < lines2pop; ++i)
	{
		String line = SDreadline(file);
		call(line);
	}

	



	
	//SDreadline(file);
}








//////////////////////////////////////////////////////  HIGHER LEVEL LOGGING FUNCTIONS ///////////////////////////////////////////////////
